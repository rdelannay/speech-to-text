# Petite app speech to text

Setup pour le faire fonctionner : 

- `python3 -m venv venv`
- `source venv/bin/activate`
- `pip install vosk`
- `pip install pyaudio`
- `pip install sounddevice`

(attention à la version de pip)

- `cd vosk-api/python/example/`
- `python3 test_enregistrements.py --model="Model" --textfile="enregistrement.txt" --filename="enregistrement.wav"`

Le fichier `test_enregistrements.py` sert à tester le micro et à enregistrer les captations dans un txt et un audio en wav

- `--model="Model"` récupère le modèle fr disponible sur vosk (je l'ai récupéré pour l'avoir en local)
- `--textfile` passe le nom du fichier txt en argument
- `--filename` passe le nom du fichier audio en argument (ne fonctionne pas)

Les résultats des voix captées passent dans le terminal : Vosk construit un JSON dans lequel il y a des partials : les partials sont affichés dans le terminal et sont de bonne qualité.
Les résultats finaux enregistrés dans le txt sont de piètre qualité (à améliorer).
